@echo off

cd %~dp0

call mvnw install:install-file -Dfile=lib/openrules-core-8.3.1.jar -DpomFile=lib/openrules-core-8.3.1.pom
call mvnw install:install-file -Dfile=lib/openrules-pom-8.3.1.pom -DpomFile=lib/openrules-pom-8.3.1.pom
call mvnw install:install-file -Dfile=lib/openrules-aws-8.3.1.jar -DpomFile=lib/openrules-aws-8.3.1.pom
call mvnw install:install-file -Dfile=lib/openrules-build-8.3.1.jar -DpomFile=lib/openrules-build-8.3.1.pom
call mvnw install:install-file -Dfile=lib/openrules-explorer-8.3.1.jar -DpomFile=lib/openrules-explorer-8.3.1.pom
call mvnw install:install-file -Dfile=lib/openrules-tools-8.3.0.jar -DpomFile=lib/openrules-tools-8.3.0.pom
call mvnw install:install-file -Dfile=lib/openrules-plugin-8.3.1.jar -DpomFile=lib/openrules-plugin-8.3.1.pom
call ext.bat
echo.
echo OpenRules Decision Manager 8.3.1    INSTALLATION COMPLETED
pause
