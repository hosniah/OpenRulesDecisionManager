package vacation.days;

import com.openrules.core.DecisionModel;
import com.openrules.core.Goal;

public class Launcher {
    
    public static void main(String[] args) {
        System.out.println("vacation.days.Launcher");
        long startTime = System.currentTimeMillis();
        DecisionModel model = new DecisionModelVacationDays();
        //model.setDebug(true);
        Goal goal = model.createGoal();
        
        Employee employee = new Employee();
        employee.setId("Mary Grant");
        employee.setAge(46);
        employee.setService(18);
        goal.use("Employee", employee);
        goal.execute();
        System.out.println("Vacation Days = " + employee.getVacationDays());
        System.out.println(employee);
        long endTime = System.currentTimeMillis();
        System.out.println("Total Elapsed time: " + (endTime - startTime +1) + " ms");
    }

}
