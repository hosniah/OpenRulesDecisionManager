package vacation.days;

import com.openrules.core.DecisionModel;
import com.openrules.core.Goal;


public class Launcher2 {
    
    public static void main(String[] args) {
        
        DecisionModel model = new DecisionModelVacationDays();
        Goal goal = model.createGoal();
        
        Employee employee = new Employee ();
        employee.setId("Mary Grant");
        employee.setAge(46);
        employee.setService(18);
        goal.use("Employee", employee);
        goal.execute();
        System.out.println("Vacation Days = " + employee.getVacationDays());
        System.out.println(employee);
        
     // Try another goal for the same model
        Goal goal5 = model.createGoal("Eligible for Extra 5 Days");
        goal5.put("Report", "On");
        Employee emp5 = new Employee();
        emp5.setId("Barry Smith");
        emp5.setAge(25);
        emp5.setAge(2);
        goal.use("Employee", emp5);
        goal5.use("Employee", emp5);
        goal5.execute();
        System.out.println("Eligible for Extra 5 Days = " + emp5.isEligibleForExtra5Days());
        
    }

}
