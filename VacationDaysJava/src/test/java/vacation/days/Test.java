package vacation.days;

import com.openrules.core.DecisionModel;
import com.openrules.core.Goal;

public class Test {
    
    public static void main(String[] args) {
           
        DecisionModel model = new DecisionModelVacationDays();
        Goal goal = model.createGoal();
        goal.put("Report", "On");
        
        Object[] employees = employeesArray.get();
        int n=0;
        for (Object employee : employees) {
            System.out.println("\n=== Test employees[" + n + "] ===");
            long startTime = System.currentTimeMillis();
            goal.use("Employee", employee);
            goal.execute();
            System.out.println(employee.toString());
            n++;
            long endTime = System.currentTimeMillis();
            System.out.println("Elapsed time: " + (endTime - startTime +1) + " ms");
        }        
    }

}
