package vacation.days;

import com.openrules.core.DecisionModel;
import com.openrules.core.Goal;

public class Main {
    
    public static void main(String[] args) {
        DecisionModel model = new DecisionModelVacationDays();
        Goal goal = model.createGoal();
        
        Employee employee = new Employee ();
        employee.setId("Mary Grant");
        employee.setAge(46);
        employee.setService(18);
        goal.use("Employee", employee);
        goal.execute();
        System.out.println("Vacation Days = " + employee.getVacationDays());
    }
}
