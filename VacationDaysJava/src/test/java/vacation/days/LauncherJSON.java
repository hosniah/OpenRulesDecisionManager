package vacation.days;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openrules.core.DecisionModel;
import com.openrules.core.Goal;


public class LauncherJSON {
    
    private static ObjectMapper mapper = new ObjectMapper(); 
    
    public static void main(String[] args) throws Exception {
        
        DecisionModel model = new DecisionModelVacationDays();
        Goal goal = model.createGoal();
        
        File jsonIn = new File("data/employee.json");
        Employee employee = mapper.readValue(jsonIn, Employee.class);
        goal.use("Employee", employee);
        goal.execute();
        System.out.println("Vacation Days = " + employee.getVacationDays());
        File jsonOut = new File("data/employeeResponse.json");
        mapper.writerWithDefaultPrettyPrinter().writeValue(jsonOut, employee);
        System.out.println(employee);
    }

}
