package vacation.days;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openrules.core.DecisionModel;
import com.openrules.core.Goal;


public class LauncherJsonRequest {
    
    private static ObjectMapper mapper = new ObjectMapper(); 
    
    public static void main(String[] args) throws Exception {
        
        Employee[] employees = mapper.readValue(new File("data/Request.json"), Employee[].class);
        List<Employee> response = new ArrayList<Employee>();
        
        DecisionModel model = new DecisionModelVacationDays();
        Goal goal = model.createGoal();
        
       for (Employee employee: employees) {
        goal.use("Employee", employee);
        goal.execute();
        System.out.println("Vacation Days = " + employee.getVacationDays());
        System.out.println(employee);
        response.add(employee);
       }
       
       mapper.writerWithDefaultPrettyPrinter().writeValue(new File("data/Response.json"), response);
    }

}
