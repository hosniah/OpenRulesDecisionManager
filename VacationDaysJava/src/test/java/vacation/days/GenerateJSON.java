package vacation.days;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;


public class GenerateJSON {
    
    private static ObjectMapper mapper = new ObjectMapper(); 
    
    public static void main(String[] args) throws Exception {
        
        Employee[] employees = employeesArray.get();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File("data/Request.json"), employees);
        
      }

}
