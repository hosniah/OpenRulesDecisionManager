package vacation.days;

import com.openrules.core.DecisionModel;
import com.openrules.core.ExecutedRule;
import com.openrules.core.ExecutionListener;
import com.openrules.core.Goal;


public class MainWithExecutedRules {
    
    public static void main(String[] args) {
        DecisionModel model = new DecisionModelVacationDays();
        Goal goal = model.createGoal();
        
        Employee employee = new Employee();
        employee.setId("Mary Grant");
        employee.setAge(46);
        employee.setService(18);
        goal.use("Employee", employee);
        
        ExecutionListener listener = new ExecutionListener();
        goal.addListener(listener);
        
        goal.execute();

        for( ExecutedRule r : listener.getExecutedRules() ) {
            //System.out.println(r);
            System.out.println("Executed Rule: " + r.getDecisionTableName() + " #" + r.getRuleNumber()); // + " ("+r.getRuleRange() + ")" );
            System.out.println("\t" + r.ruleText());
            if ( r.getVariables() != null && !r.getVariables().isEmpty() ) {
                System.out.println( "\tVariables :" );
                r.getVariables().forEach( (key,value) -> {
                    System.out.println("\t\t"+key + ":  old=" + value[0] + ", new=" + value[1] );
                });
            }
        }
        System.out.println("Vacation Days = " + employee.getVacationDays());
    }
}
