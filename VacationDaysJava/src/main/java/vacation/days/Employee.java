package vacation.days;

public class Employee {

    private String id;
    private int age;
    private int service;
    private int vacationDays;
    private boolean eligibleForExtra5Days;
    private boolean eligibleForExtra3Days;
    private boolean eligibleForExtra2Days;
    
    
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    
    public void setVacationDays(int vacationDays) {
        this.vacationDays = vacationDays;
    }
    public int getVacationDays() {
        return this.vacationDays;
    }
    
    public void setEligibleForExtra5Days(boolean eligibleForExtra5Days) {
        this.eligibleForExtra5Days = eligibleForExtra5Days;
    }
    public boolean isEligibleForExtra5Days() {
        return this.eligibleForExtra5Days;
    }
    
    public void setEligibleForExtra3Days(boolean eligibleForExtra3Days) {
        this.eligibleForExtra3Days = eligibleForExtra3Days;
    }
    public boolean isEligibleForExtra3Days() {
        return this.eligibleForExtra3Days;
    }
    
    public void setEligibleForExtra2Days(boolean eligibleForExtra2Days) {
        this.eligibleForExtra2Days = eligibleForExtra2Days;
    }
    public boolean isEligibleForExtra2Days() {
        return this.eligibleForExtra2Days;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    public int getAge() {
        return this.age;
    }
    
    public void setService(int service) {
        this.service = service;
    }
    public int getService() {
        return this.service;
    }
    @Override
    public String toString() {
        return "Employee [id=" + id + ", age=" + age + ", service=" + service + ", vacationDays=" + vacationDays
                + ", eligibleForExtra5Days=" + eligibleForExtra5Days + ", eligibleForExtra3Days="
                + eligibleForExtra3Days + ", eligibleForExtra2Days=" + eligibleForExtra2Days + "]";
    }
    
}
