package vacation.days.spring;

import com.openrules.core.util.CLIParser;

public class TestLocalServer {
    
    public static void main(String[] args) throws Exception {
        
	String endpoint = "http://localhost:8080/vacation-days";        

        CLIParser.Args  cliArgs = new CLIParser().parse(args);
        if ( cliArgs.getArguments().size() < 1 ) {
            System.out.println("Missing required argument: endpoint URL");
            System.out.println("\nTest will use endpoint " + endpoint + "\n");
        } else {
            endpoint = cliArgs.getArguments().get(0);
        }

        System.out.println("Running tests for DecisionModelVacationDays decision service at " + endpoint);

        Employee[] employees = employeesArray.get();
        
        DecisionModelVacationDaysClient client = new DecisionModelVacationDaysClient(endpoint);
        
        for (Employee employee : employees) {
            System.out.println("\nCalculate Vacation Days for Employee " + employee.getId());
            DecisionModelVacationDaysRequest request = new DecisionModelVacationDaysRequest();
            request.setEmployee(employee);
            long startTime = System.currentTimeMillis();
            DecisionModelVacationDaysResponse response = client.execute(request);
            long endTime = System.currentTimeMillis();
            System.out.println("Result: " + response.getEmployee().getVacationDays() + " days");
            System.out.println("Elapsed time: " + (endTime - startTime +1) + " mills");
        }
    }
}
