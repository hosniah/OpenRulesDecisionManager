package vacation.days.spring;

import java.util.Random;
import com.openrules.core.util.CLIParser;

public class TestBatchLocalServer {

    public static void main(String[] args) throws Exception {

        String endpoint = "http://localhost:8080/vacation-days/batch";        

        CLIParser.Args  cliArgs = new CLIParser().parse(args);
        if ( cliArgs.getArguments().size() < 1 ) {
            System.out.println("Missing required argument: endpoint URL");
            System.out.println("\nTest will use endpoint " + endpoint + "\n");
        } else {
            endpoint = cliArgs.getArguments().get(0);
        }

        // Generate test employees with random IDs
        Random random = new Random();
        int count = 100;
        Employee[] all = new Employee[count];
        for (int i = 0; i < count; ++i) {
            Employee e = new Employee();
            e.setId("id_" + random.nextInt());
            e.setAge(20 + random.nextInt(50));
            e.setService(random.nextInt(50));
            all[i] = e;
        }

        // Create a client for batch processing endpoint
        DecisionModelVacationDaysClient client = new DecisionModelVacationDaysClient(endpoint);

        // Split test-employees into batches with 200 employees in a batch and execute the batches 
        int batchSize = 200;
        long startTime0 = System.currentTimeMillis();
        int batchNumber = 1;
        for (int i = 0; i < count; i = i + batchSize) {
         // Define batch as an array of DecisionModelVacationDaysRequest(s)
            int size = Math.min(batchSize, count - i);
            DecisionModelVacationDaysRequest[] batch = new DecisionModelVacationDaysRequest[size];
            for (int j = 0; j < size; ++j) {
                batch[j] = new DecisionModelVacationDaysRequest();
                batch[j].setEmployee(all[i + j]);
            }
            // Execute batch
            long startTime = System.currentTimeMillis();
            client.executeBatch(batch);
            long endTime = System.currentTimeMillis();
            System.out.println("Batch " + batchNumber + " executed in " + (endTime - startTime) + " ms");
            batchNumber++;
        }
        long endTime0 = System.currentTimeMillis();
        double elapsedTime = (endTime0 - startTime0) / 1000;
        System.out.println("Total Elapsed time: " + elapsedTime + " seconds");
        System.out.println("Requests Per Second: " + (count / elapsedTime));
        System.out.println("Average time per employee: " + (elapsedTime / count));
    }
}
